# A currently valid, but unsolvable puzzle.
# This puzzle requires an extreme amount of time/effort by
# the recursive solver to determine it cannot be solved.
#
# Ok, so the actual puzzle at the bottom of this file isn't so extreme. I
# started with the puzzle below, which is unsolvable because the four 1s
# in the puzzle restrict the middle 3x3 square to such that its 1 must be
# in the very center. However, that spot is already filled. I let my code
# run overnight and it was still trying when I stopped it. Think of all
# the backtracking it would have to do before realizing it must fail!!
#
# 0 0 0 0 0 0 0 0 0
# 0 0 0 0 0 0 0 0 0
# 0 0 0 1 0 0 0 0 0
# 0 0 1 0 0 0 0 0 0
# 0 0 0 0 2 0 0 0 0
# 0 0 0 0 0 0 1 0 0
# 0 0 0 0 0 1 0 0 0
# 0 0 0 0 0 0 0 0 0
# 0 0 0 0 0 0 0 0 0
#
# So I replaced the 2 in the middle with a 1 and quickly got this solution:
#
# 1 2 3 4 5 6 7 8 9
# 4 5 6 7 8 9 2 1 3
# 7 8 9 1 2 3 4 5 6
# 2 3 1 5 4 7 6 9 8
# 5 6 4 9 1 8 3 2 7
# 8 9 7 3 6 2 1 4 5
# 3 4 5 6 9 1 8 7 2
# 6 1 2 8 7 5 9 3 4
# 9 7 8 2 3 4 5 6 1
#
# I then removed the values in the middle 3x3 square, put the 2 back in the
# middle to again make the puzzle unsolvable, and then cleared the first two
# rows and a few other cells so the puzzle was valid and there was at least
# a little work to be done. As is, the solver fails in 20-30 seconds. Delete
# the 8 from the third row and it takes well over a minute. Delete the other
# values in the third row and it takes longer than I wanted to wait.
#
0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0
0 8 0 1 0 3 0 5 0
2 3 1 0 0 0 6 9 8
5 6 4 0 2 0 3 0 7
8 9 7 0 0 0 1 4 5
3 4 5 6 9 1 8 7 2
6 1 2 8 7 5 9 3 4
9 7 8 2 3 4 5 6 1
